package com.devcamp.countryregionapi.service;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.devcamp.countryregionapi.model.Country;

@Service
public class CountryService {
    @Autowired
    private RegionService regionService;
    ArrayList<Country> allCountryMain = new ArrayList<>();

    Country vietnam = new Country("VN","Viet Nam");
    Country us = new Country("US","America");
    Country russia = new Country("VN","Russia");

    public ArrayList<Country> getAllCountries(){
        ArrayList<Country> allCountry = new ArrayList<>();

        vietnam.setRegions(regionService.getRegionVietNam());
        us.setRegions(regionService.getRegionUS());
        russia.setRegions(regionService.getRegionRussia());

        allCountry.add(vietnam);
        allCountry.add(us);
        allCountry.add(russia);
        allCountryMain.addAll(allCountry);
        return allCountry;
    }
    public ArrayList<Country> filterQuery(String queryString){
        ArrayList<Country> result = new ArrayList<>();

        for (Country contryCode : allCountryMain) {
            if(contryCode.contains(queryString)){
                result.add(contryCode);
            }
        }

        return result;
    }
    public Country filterIndex(int indexParam){
        Country result = new Country();

        if(indexParam >= 0 && indexParam <= 2){
            result = allCountryMain.get(indexParam);
        }
        return result;
    }
}

