package com.devcamp.countryregionapi.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.countryregionapi.model.Region;

@Service
public class RegionService {
    Region hanoi = new Region("HN","Ha noi");
    Region hochiminh = new Region("HCM","Ho Chi Minh");
    Region danang = new Region("DN","Da Nang");

    Region newyorks = new Region("NY","New Yorks");
    Region florida = new Region("FLO","Florida");
    Region texax = new Region("TX","Texax");

    Region moscow = new Region("MC","Moscow");
    Region kaluga = new Region("KL","Kaluga");
    Region sainpeterburg = new Region("SP","Sainpeterburg");

    public ArrayList<Region> getRegionVietNam(){
        ArrayList<Region> regionVietNam = new ArrayList<>();

        regionVietNam.add(hanoi);
        regionVietNam.add(hochiminh);
        regionVietNam.add(danang);

        return regionVietNam;
    }
    public ArrayList<Region> getRegionUS(){
        ArrayList<Region> regionUS = new ArrayList<>();

        regionUS.add(newyorks);
        regionUS.add(florida);
        regionUS.add(texax);

        return regionUS;
    }
    public ArrayList<Region> getRegionRussia(){
        ArrayList<Region> regionRussia = new ArrayList<>();

        regionRussia.add(moscow);
        regionRussia.add(kaluga);
        regionRussia.add(sainpeterburg);

        return regionRussia;
    }
    public Region filterRegions(String regionCode){
        ArrayList<Region> allRegion = new ArrayList<>();

        allRegion.add(hanoi);
        allRegion.add(hochiminh);
        allRegion.add(danang);
        allRegion.add(newyorks);
        allRegion.add(florida);
        allRegion.add(texax);
        allRegion.add(moscow);
        allRegion.add(kaluga);
        allRegion.add(sainpeterburg);
         
        Region findRegion = new Region();

        for (Region regionElement : allRegion) {
            if(regionElement.getRegionCode().equals(regionCode)){
                findRegion = regionElement;
            }
        }

        return findRegion;
    }
}
